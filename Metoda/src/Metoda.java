import java.util.Scanner;

public class Metoda {
    public static void main(String[] args) {
        System.out.println("Metody:  startsWith(), endsWith() a  contains()" );
        // Zde si ukážeme příklad metod výše na slově Krokonosohroch.
        String s = "Krokonosohroch";
        System.out.println(s.startsWith("krok"));
        System.out.println(s.endsWith("hroch"));
        System.out.println(s.contains("nos"));
        System.out.println(s.contains("roh"));

        // Metoda isEmpty()
        System.out.println("Metoda: isEmpty()");
        String s1 = " ";
        String s2 = "";
        String s3 = "text";
        System.out.println(s1.isEmpty());
        System.out.println(s2.isEmpty());
        System.out.println(s3.isEmpty());

        // Metody: toUpperCase() a toLoweCase()
        System.out.println("Metody: toUpperCase() a toLoweCase");
        String konfig = "Fullscreen shaDows autosave";
        konfig = konfig.toLowerCase();
        System.out.println("Poběží hra ve fullsreenu?");
        System.out.println(konfig.contains("fullscreen"));
        System.out.println("Budou zapnuté stíny?");
        System.out.println(konfig.contains("shadows"));
        System.out.println("Přeje si hráč vypnout zvuk?");
        System.out.println(konfig.contains("nosound"));
        System.out.println("Přeje si hráč hru automaticky ukládat?");
        System.out.println(konfig.contains("autosave"));
        // Vidíme, že jsme schopni zjistit přítomnost jednotlivých slov v řetězci
        //tak, že si nejprve řetězec převedeme celý na malá písmena (nebo na
        //velká) a potom kontrolujeme přítomnost slova jen malými (nebo velkými)
        //písmeny.

        /**Metoda trim()
        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.println("Metoda: trim()");
        System.out.println("Zadejte cele číslo:");
        String s4 = sc.nextLine();
        System.out.println("Zadal jste text:" + s4);
        System.out.println("Text po funkci trim: " + s4.trim());
        int b = Integer.parseInt(s.trim());
        System.out.println("Převedl jsem zadaný text na číslo parsováním, zadal jste: " + b); */

        //Metoda replace()
        String s5 = "Kočky jsou nejlepší!";
        s5 = s5.replace("Kočky", "Psi");
        System.out.println(s5);

        //Metoda format
        int a = 10;
        int b = 20;
        int c = a + b;
        String s6 = String.format("Když sečteme %d a %d, dostaneme %d", a, b, c);
        System.out.println(s6);

        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.println("Zadejte jakékoliv slovo: ");
        String slovo = sc.nextLine();
        System.out.printf("Délka zadaného slova je: %d", slovo.length());


    }
}
import java.util.Scanner;

/**
 * Pronačítání z konzole slouží metoda nextLine().
 * Její využití bude v budoucnu časté.
 *
 * Třída java.util.Scanner - Umužní přístup k metodám pro vstup z konzole. Moderní IDE ji doplní v některých případech za vás
 *
 * String vstup; deklaruje textový řetězec vstup. Do vstup se přiřadí hodnota z metody nextLine().
 * To co človek zadá do konzole.
 *
 * Spojování řetězců. Pomocí operátorů + můžeme  spojit několik textových řetězců.
 * Může být proměnná nebo text v "uvozovkách".
 *
 * @author kankys
 */
public class Opakovac {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.println("Nazdar ptáku, jsem opakovač a vše po tobě zopakuji!");
        System.out.println("Zkus jsem něco pařádkem napsat: ");
        String vstup;
        vstup = sc.nextLine();
        String vystup;
        vystup = vstup + ", " + vstup + "! ";
        System.out.println(vystup);
    }
}
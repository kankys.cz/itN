public class Boolean {
    public static void main(String[] args) {
        boolean b = false;
        boolean vyraz = (15 > 5);
        System.out.println(b);
        System.out.println(vyraz);
        /**
         * Vidíme, že výraz nabývá hodnoty
         * true (pravda), protože 15 je opravdu větší než 5.
         * @author kankys
         */
    }
}
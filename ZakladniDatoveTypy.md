

Celá čísla: 

int

Desetinná čísla: 

float

Textový řetězec: 

String (String píšeme s velkým písmenem na začátku.)

.....................................................................................

**Datový typ:**

Rozsah:
Velikost:

byte / -128 až 127  / 8 bitů

short / -32 768 až 32 767 / 16 bitů

int / -2 147 483 648 až 2 147 483 647 / 32 bitů

long / -9 223 372 036 854 775 808 až 9 223 372 036 854 775 807 / 64 bitů

..........................................................................................

**Desetinná čísla:**

Datový typ:
Rozsah:
Přesnost:

float / +-1.5 * 10−45 až +-3.4 * 1038 / 7 čísel

double / +-5.0 * 10−324 až +-1.7 * 10308 / 15-16 čísel

Když do typu float chceme dosadit přímo ve zdrojovém kódu,
musíme použít sufix F, u double sufix D:

float f = 3.14F;

double d = 2.72;

U double ho můžeme vypustit, jelikož je
výchozím desetinným typem.

Jako desetinný separátor používáme ve zdrojovém kódu vždy tečku,
nehledě na to, jaké máme v operačním systému regionální nastavení.

.....................................................................................................

**Další vestavěné datové typy:**

Datový typ char:

Datový typ:
Rozsah:
Velikost/Přesnost:

char / U+0000 až U+ffff / 16 bitů

boolean / true nebo false / 8 bitů

Typ char nám reprezentuje jeden znak, na rozdíl od
String, který reprezentoval celý řetězec znaků. Znaky v Javě
píšeme do apostrofů:

char c = 'A';

Typ char patří v podstatě do celočíselných proměnných
(obsahuje číselný kód znaku)

.........................................................................

Datový typ BigDecimal:

Typ BigDecimal řeší problém ukládání desetinných čísel
v binární podobě, ukládá totiž číslo vnitřně jako pole.
Používá se tedy pro uchování peněžních hodnot.

..................................................................................

V Javě jsou čísla tzv. odděděna od třídy
**Number**. To je spíše informace do budoucna. Jelikož nyní
nevíme, co dědičnost znamená, důležitá pro nás není.
**Number** obsahuje ještě čtyři podtřídy, kterými se nebudeme
podrobněji zabývat. **BigDecimal** a **BigInteger** slouží
k výpočtům s vysokou přesností. Třídy **AtomicInteger** a
**AtomicLong** se používají v aplikacích s více podprocesy. Opět
je důležité, abyste věděli, že něco takového existuje.

...................................................................................................

**Datový typ boolean:**

Typ boolean nabývá dvou hodnot: true (pravda) a
false (nepravda). Budeme ho používat zejména tehdy, až se
dostaneme k podmínkám. Do proměnné typu boolean lze uložit jak
přímo hodnotu true/false, tak i logický výraz.
Príklad: viz. aplikace boolean.java

......................................................................................................


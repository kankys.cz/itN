Referenční typy jsou složitější, než ty primitivní. Jeden
takový typ již známe, je jím String. Možná vás napadá, že
String nemá nijak omezenou délku, je to tím, že s
referenčními typy se v paměti pracuje jinak. Hodnotové typy
začínají na rozdíl od typů primitivních velkým písmenem.


**Datový tyo String**

String má na sobě řadu opravdu užitečných metod.

**Metody: startsWith(), endsWith() a ontains()**

Můžeme se jednoduše zeptat, zda řetězec začíná, končí nebo zda
obsahuje určitý podřetězec (substring). Podřetězcem myslíme část
původního řetězce. Všechny tyto metody budou jako parametr brát
samozřejmě podřetězec a vracet hodnoty typu boolean
(true/false).

(více v aplikaci Metoda.java)

**Metoda isEmpty()**

Někdy se nám hodí vědět, zda je řetězec prázdný. To znamená, že
jeho délka je 0 a neobsahuje žádný znak, ani např. mezeru.
Takový řetězec můžeme získat např. tak, že uživatel nic nezadá do
nějakého vstupu. Metoda isEmpty() nám vrátí true
pokud je řetězec prázdný a false pokud není.

**Metody toUpperCase() a toLowerCase()**

Rozlišování velkých a malých písmen může být někdy na obtíž.
Mnohdy se budeme potřebovat zeptat na přítomnost podřetězce tak, aby
nezáleželo na velikosti písmen. Situaci můžeme vyřešit pomocí metod
toUpperCase() a toLowerCase(), které vrací řetězec
ve velkých a v malých písmenech. Uveďme si reálnější příklad než je
Krokonosohroch. Budeme mít v proměnné řádek konfiguračního souboru,
který psal uživatel. Jelikož se na vstupy od uživatelů nelze spolehnout,
musíme se snažit eliminovat možné chyby, zde např. s velkými písmeny:

(viz. Metoda.java)

**Metoda: trim()**

Další nástrahou mohou být mezery a obecně všechny tzv. bílé znaky,
které nejsou vidět, ale mohou nám uškodit. Obecně může být dobré
trimovat všechny vstupy od uživatele.
Odstraňují se vždy bílé znaky kolem řetězce, nikoli
uvnitř:

(viz. MetodaTrim.java)

**Metoda replace**

Asi nejdůležitější metodou pro String je nahrazení
určité jeho části jiným textem. Jako parametry zadáme dva podřetězce,
jeden co chceme nahrazovat a druhý ten, kterým to chceme nahradit. Metoda
vrátí nový String, ve kterém proběhlo nahrazení. Když daný
podřetězec metoda nenajde, vrátí původní řetězec.
(viz.Metoda.java)


**Metoda format()**

Metoda format() je velmi užitečná metoda, která nám
umožňuje vkládat do samotného textového řetězce zástupné značky. Ty
jsou reprezentovány jako procento a zkratka datového typu. Metoda se volá na
typu String, prvním parametrem je textový řetězec se značkami,
další dále následují proměnné v tom pořadí, v kterém se mají do textu
místo značek vložit. Všimněte si, že se metoda nevolá na konkrétní
proměnné (přesněji instanci, viz další díly), ale přímo na typu
String.

Značky jsou:


	%d pro celá čísla,

	%s pro String,

	%f pro float. 

U float můžeme  definovat délku desetinné části, např: %.2f pro dvě desetinná místa.

Konzole sama umí přijímat text v takovémto formátu, jen musíme místo
println() volat printf().

**Metoda length()**

Poslední, ale nejdůležitější je length(), tedy délka.
Vrací celé číslo, které představuje počet znaků v řetězci.

(viz. Metoda.java)











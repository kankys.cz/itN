import java.util.Scanner;

public class BlokPrikaz {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.print("Zadej čísllko na výpočet odmocniny:");
        int a = Integer.parseInt(sc.nextLine()); // do proměnné vloží číslo z konzole
        if (a >= 0) {
            System.out.println("Zadal jsi číslo větší nebo rovno 0, to znamená, že ho mohu odmocnit!");
            double o = Math.sqrt(a);
            System.out.println("Odmocnina z čísla " + a + " je " + o);
        }
        System.out.println("Dekuji za použití :-)");

    }
    }



import java.util.Scanner;

public class Else {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.println("Zadej nějaké číslo, ze kterého spočítám odmocninu:");
        int a = Integer.parseInt(sc.nextLine());
        if (a >= 0) {
            System.out.println("Zadal jsi číslo větší nebo rovno 0, to znamená, že ho mohu odmocnit!");
            double o = Math.sqrt(a); // zde se odmocní hodnota a
            System.out.println("Odmocnina z čísla " + a + " je " + o);
        }
        else {
            System.out.println("Odmocnina ze záporného čísla neni možná!");
        }
        System.out.println("Děkuji za zadání.");
    }
}

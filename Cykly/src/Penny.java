/**
 * Cyklus proběhne 3x, zpočátku je v proměnné i nula, cyklus
 * vypíše "Knock" a zvýší proměnnou i o jedna. Poté běží
 * stejně s jedničkou a dvojkou. Jakmile je v i trojka, již
 * nesouhlasí podmínka i < 3 a cyklus končí. O vynechávání
 * složených závorek platí to samé, co u podmínek. V tomto případě tam
 * nemusí být, protože cyklus spouští pouze jediný příkaz. Nyní můžeme
 * místo trojky napsat do deklarace cyklu desítku.
 */

public class Penny {
    public static  void main(String[] args) {
        for (int i=0; i < 3; i++) {
            System.out.println("Knock");
        }
        System.out.println("Penny!");

    }
}

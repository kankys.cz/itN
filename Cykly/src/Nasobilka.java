import java.util.Scanner;

public class Nasobilka {
    public static void main(String[] args) {
        System.out.println("Malá násobilka pomocí dvou cyklů: ");
        for (int j = 1; j <= 10; j++) {
            for (int i = 1; i <= 10; i++) {
                System.out.print((i * j) + " "); // zde pozor na print pokud zadáme println vše bude pod sebou
            }
            System.out.println();
        }
    }
}

/**
 * Použití printf(), můžeme místo ní
 * použít pouze print(), která na rozdíl od println()
 * po vypsání neodřádkuje:
 */
public class Rada2 {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            System.out.print(i + " ");
        }
    }
}

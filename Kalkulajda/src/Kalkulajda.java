import java.util.Scanner;

/**
 * Zde jsme si převedli opak k parsování, převést cokoliv do textové podoby.
 * @author kankys
 */
public class Kalkulajda {
    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in, "UTF-8");
        System.out.println("Čauves, jsem tvá kalkulajda a ráda ti to spočítám");
        System.out.println("Zadej první číslo:");
        float a = Float.parseFloat(sc.nextLine());
        System.out.println("Zadej druhé číslo:");
        float b = Float.parseFloat(sc.nextLine());
        float soucet = a + b;
        float rozdil = a - b;
        float soucin = a * b;
        float podil = a / b;
        System.out.println("Součet: " + soucet);
        System.out.println("Rozdíl: " + rozdil);
        System.out.println("Součin: " + soucin);
        System.out.println("Podíl: " + podil);
        System.out.println("Tady to máš spočítané!");

    }
}
import java.util.Scanner;

public class Kalkulacka {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.println("Vítej v kalkulačce");
        System.out.println("Zadej první číslo:");
        float a = Float.parseFloat(sc.nextLine());
        System.out.println("Zadejte druhé číslo:");
        float b = Float.parseFloat(sc.nextLine());

        System.out.println("Zvolte si operaci:");
        System.out.println("1 - sčítání");
        System.out.println("2 - odčítání");
        System.out.println("3 - násobení");
        System.out.println("4 - dělení");

        int volba = Integer.parseInt(sc.nextLine());
        float vysledek = 0;

        if (volba == 1) {
            vysledek = a + b;
        } else if (volba == 2) {
            vysledek = a - b;
        } else if (volba == 3) {
            vysledek = a * b;
        } else if (volba == 4) {
            vysledek = a / b;
        }

        if ((volba > 0) && (volba < 5)) { // nastavení rozpětí volby čísla
            System.out.println("Výsledek: " + vysledek);
        } else {
            System.out.println("Neplatná volba!");
        }
        System.out.println();
        System.out.println("Díky za využití kalkulačky");
    }
}

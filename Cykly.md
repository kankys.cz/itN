**Cykly**

Jak již slovo cyklus napoví, něco se bude opakovat. Když chceme v
programu něco udělat 100x, jistě nebudeme psát pod sebe 100x ten samý kód,
ale vložíme ho do cyklu. Cyklů máme několik druhů, vysvětlíme si, kdy
který použít. Samozřejmě si ukážeme praktické příklady.

**Cyklus for**

Tento cyklus má stanovený pevný počet opakování a
hlavně obsahuje tzv. řídící proměnnou (celočíselnou),
ve které se postupně během běhu cyklu mění hodnoty. Syntaxe (zápis) cyklu
for je následující:

for (promenna; podminka; prikaz)


	promenna je řídící proměnná cyklu, které
	nastavíme počáteční hodnotu (nejčastěji 0, protože v
	programování vše začíná od nuly, nikoli od jedničky). Např. tedy
	int i = 0. Samozřejmě si můžeme proměnnou i
	vytvořit někde nad cyklem a už nemusíme psát slovíčko int,
	bývá ale zvykem používat právě int i.

	podminka je podmínka vykonání dalšího kroku cyklu.
	Jakmile nebude platit, cyklus se ukončí. Podmínka může být např
	(i < 10).

	prikaz nám říká, co se má v každém kroku s
	řídící proměnnou stát. Tedy zda se má zvýšit nebo snížit. K tomu
	využijeme speciálních operátorů ++ a --, ty
	samozřejmě můžete používat i úplně běžně mimo cyklus, slouží ke
	zvýšení nebo snížení proměnné o 1.


**Příklady užití cyklu**

Pojďme si udělat několik jednoduchých příkladů na procvičení
for cyklu.

**Klepání na dveře**

Většina z nás jistě zná Sheldona z The Big Bang Theory. Pro ty co ne,
budeme simulovat situaci, kdy klepe na dveře své sousedky. Vždy 3x zaklepe a
poté zavolá: "Penny!".
(viz. Penny.java složka Cykly)

Cyklus proběhne 3x, zpočátku je v proměnné i nula, cyklus
vypíše "Knock" a zvýší proměnnou i o jedna. Poté běží
stejně s jedničkou a dvojkou. Jakmile je v i trojka, již
nesouhlasí podmínka i < 3 a cyklus končí. O vynechávání
složených závorek platí to samé, co u podmínek. V tomto případě tam
nemusí být, protože cyklus spouští pouze jediný příkaz. Nyní můžeme
místo trojky napsat do deklarace cyklu desítku.

**Řada**

Příkaz se spustí 10x aniž bychom psali něco navíc. Určitě vidíte,
že cykly jsou mocným nástrojem.

Zkusme si nyní využít toho, že se nám proměnná inkrementuje. Vypišme
si čísla od jedné do deseti a za každým mezeru:
(viz. Rada.java složka Cykly)

Vidíme, že řídící proměnná má opravdu v každé iteraci (průběhu)
jinou hodnotu.

Pokud vás zmátlo použití printf(), můžeme místo ní
použít pouze print(), která na rozdíl od println()
po vypsání neodřádkuje:

(viz. Rada2.java složka Cykly)

**Malá násobilka**

Poměrně zásadní rozdíl, že? Pochopitelně nemůžeme
použít u obou cyklů i, protože jsou vložené do sebe.
Proměnná j nabývá ve vnějším cyklu hodnoty 1
až 10. V každé iteraci (rozumějte průběhu) cyklu je poté
spuštěn další cyklus s proměnnou i. Ten je nám již známý,
vypíše násobky, v tomto případě násobíme proměnnou j. Po
každém běhu vnitřního cyklu je třeba odřádkovat, to vykoná
System.out.println().
(viz. Nasobilka složka Cykly)

**Mocnina čísla**








import java.util.Scanner;

/**
 * @author kankys
 */
public class Pětinasobovac {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in, "UTF-8");
        /**
         * Pokud budete mít problémy s "UTF-8" zkuste "Windows-1250".
         */
        System.out.println("Zadej číslo a já ti ho zpětinásobím:");
        String s = sc.nextLine(); // zde voláme z konzole. Vámi vložené číslo se uloží do hodnoty s.
        int a = Integer.parseInt(s); // zde říkáme že výstup z konzole je číslo. Tazvaně parsujeme. Volá se pouze na třídě Integer.
        a = a * 5;
        System.out.println(a);

        /**
         * Schválně si zkuste místo čísla v tomto programu zadat písmeno.
         */
    }
}
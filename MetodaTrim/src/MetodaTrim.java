


import java.util.Scanner;
public class MetodaTrim {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in,"UTF-8");
        System.out.println("Metoda: trim()");
        System.out.println("Zadejte cele číslo:");
        String s = sc.nextLine();
        System.out.println("Zadal jste text:" + s);
        System.out.println("Text po funkci trim: " + s.trim());
        int a = Integer.parseInt(s.trim());
        System.out.println("Převedl jsem zadaný text na číslo parsováním, zadal jste: " + a);


    }
}

import java.util.Scanner;

public class Podminka3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.println("Zadejte číslo v rozmezí 10-20 nebo 30-40:");
        int a = Integer.parseInt(sc.nextLine());
        if (((a >=10) && (a <= 20)) || ((a >= 30) && ((a >= 30) && (a <= 40)))) { // tímto způsobem porovnáme dvě rozmezí čísel
            System.out.println("Zadal jsi správně");
        }
        else {
            System.out.println("Zadal jsi špatně");
        }
    }
}
/**
 * Zapamatuj si operátory && - A zároveň
 * A druhý operátor || - Nebo
 * @author kankys
 */
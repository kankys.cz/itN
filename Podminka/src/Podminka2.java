import java.util.Scanner;

public class Podminka2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.println("Zadej číslo v rozmezí 10-20:");
        int a = Integer.parseInt(sc.nextLine());
        if ((a >= 10) && (a <= 20)) {
            System.out.println("Zadal jsi správně");
        }
        else {
            System.out.println("Zasal jsi špatně");
        }
    }
}

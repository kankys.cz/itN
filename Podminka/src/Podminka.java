import java.util.Scanner;

public class Podminka {
    public static void main(String[] args) {
        if (14 > 5) //14 je větší než 5
            System.out.println("Pravda"); // pokud je podmínka splněna konzole vypíše "Pravda"
        System.out.println("Program může pokračovat dál");

       // Součástí výrazu samozřejmě může být i proměnná:

        Scanner sc = new Scanner(System.in, "UTF-8");
        System.out.println("Zadej prosím číslo:");
        try { // k tomuto se dostaneme níže
            int a = Integer.parseInt(sc.nextLine());
            if (a > 5)
                System.out.println("Zadal/a jsi číslo větší než 5!");
            else { // je nebo - porovná podmínku a podle toho vypíše na konzoli výsledek
                System.out.println("Zadal/a jsi nižšší číslo než 5 nebo rovno 5");
            }
        } catch (NumberFormatException ex) { // catch nám zkontroluje (dle zadaných parametru) jestli do konzole byl zadaný text nebo číslo v přídě textu nas upozorní že jsme nezadali číslo
            System.out.println("Nezadal jsi číslo");
        }




    }
}
/**
 * Každý první program začíná jednuducým Hello World, v mém případě "Čus Sporťák"
 * @author kankys
 */
public class CusSportak {
    public static void main(String[] args)
    {
        System.out.println("Čus Sporťák!");
    }
}


/**
 * První příkaz nám nadeklaruje novou proměnnou a datového
 * typu int, proměnná tedy bude sloužit pro ukládání celých čísel
 * Pro desetinnou proměnnou kód vypadá jako v druhém zápisu.
 * Je to téměř stejné jako s celočíselným. Jako desetinný oddělovač
 * používáme tečku a na konci desetinného čísla je nutné zadat tzv.
 * suffix F, tím říkáme, že se jedná o float.
 * @author kankys
 */
public class Program {

    public static void main(String[] args) {
        int a = 69;
        System.out.println(a);

        float b = 69.9F;
        System.out.println(b);

    }
}
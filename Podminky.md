**Podmínky větvení**

**Pomínky - if**

Podmínky zapisujeme pomocí klíčového slova if, za kterým
následuje logický výraz. Pokud je výraz pravdivý, provede se následující
příkaz. Pokud ne, následující příkaz se přeskočí a pokračuje se až
pod ním.

(viz. Podminka.java)

Pokud podmínka platí (což zde ano), provede se příkaz vypisující do
konzole text pravda.

Součástí
výrazu samozřejmě může být i proměnná.

**Relační operátory**

Ukažme si nyní relační operátory, které můžeme ve výrazech
používat:



Operátor:

C-like zápis:
		
	

	
		
			Rovnost

			==
		

		
			Je ostře větší

			>
		

		
			Je ostře menší

			<
		

		
			Je větší nebo rovno

			>=
		

		
			Je menší nebo rovno

			<=
		

		
			Nerovnost

			!=
		

		
			Obecná negace

			!




Rovnost zapisujeme dvěma == proto, aby se to nepletlo s
běžným přiřazením do proměnné, které se dělá jen jedním
=. Pokud chceme nějaký výraz znegovat, napíšeme ho do závorky
a před něj vykřičník.

**BlokPříkaz**

Když budeme chtít vykonat více než jen jeden příkaz, musíme příkazy
vložit do bloku ze složených závorek:

(viz. BlokPrikaz.java)

Často můžete vidět použití bloku i v případě, že je pod podmínkou
jen jeden příkaz, mnohdy je to totiž přehlednější.

Program načte od uživatele číslo a pokud je větší než 0,
vypočítá z něj druhou odmocninu. Mimo jiné jsme použili třídu
Math, která na sobě obsahuje řadu užitečných matematických
metod, někdy si ji blíže představíme. Metoda sqrt() vrací
hodnotu jako double.

**Větev else**

Bylo by hezké, kdyby nám program vyhuboval v případě, že zadáme
záporné číslo v předchozím případě.

Pomocí klíčového slova
else, které vykoná následující příkaz nebo blok příkazů
v případě, že se podmínka neprovede:

(viz. Else.java ve slozčce BlokPrikaz)

Kód je mnohem přehlednější a nemusíme vymýšlet opačnou podmínku,
což by v případě složené podmínky mohlo být někdy i velmi obtížné. V
případě více příkazů by byl za else opět blok
{ }.

Klíčové slovo else se také využívá v případě, kdy
potřebujeme v příkazu manipulovat s proměnnou z podmínky a nemůžeme se na
ni tedy ptát potom znovu. Program si sám pamatuje, že se podmínka nesplnila
a přejde do sekce else.

**Prohození hodnot proměnné**

Mějme číslo a, kde bude hodnota 0 nebo
1 a po nás se bude chtít, abychom hodnotu prohodili (pokud tam je
0, dáme tam 1, pokud 1, dáme tam 0). 

Nefunguje to, že? Pojďme si projet, co bude program dělat. Na začátku
máme v a nulu, první podmínka se jistě splní a dosadí do
a jedničku. No ale rázem se splní i ta druhá. Co s tím? Když
podmínky otočíme, budeme mít ten samý problém s jedničkou. Jak z toho
ven? Ano, použijeme else:
( viz. Else2.java v BlokPrikazu)

**Podmínky (větvení) podruhé: Konstrukce switch v Javě**

Java tutoriálu se naučíme skládat podmínky za pomoci
logických operátorů. Následně se podíváme na konstrukci
switch a vytvoříme jednoduchou kalkulačku.

**Skládání podmínek**

Podmínky je možné skládat, a to pomocí dvou základních
logických operátorů:

Operátor:

C-like Zápis:
		
	

	
		
			A zároveň

			&&
		

		
			Nebo

			||


**Switch**

**Složitější kalkulačka**

(viz. Kalkulacka.java složka Kalkulajda)

Konstrukce switch je převzatá z jazyka C (jako většina
gramatiky Javy). Umožňuje nám zjednodušit (relativně) zápis více
podmínek pod sebou. Vzpomeňme si na naši kalkulačku v prvních lekcích,
která načetla 2 čísla a vypočítala všechny 4
operace. Nyní si ale budeme chtít zvolit, kterou operaci chceme.

Pokud bychom
potřebovali v nějaké větvi switch spustit více příkazů,
překvapivě je nebudeme psát do bloku, ale rovnou pod sebe. Blok
{} nám zde nahrazuje příkaz break, který způsobí
vyskočení z celého switch. Switch může místo
case x: obsahovat ještě možnost default:, která se
vykoná v případě, že nebude platit žádný case.

Je jen na vás, jestli budete switch používat, obecně se
vyplatí jen při větším množství příkazů a vždy jde nahradit sekvencí
if a else. Nezapomínejte na break.
Konstrukce switch je v Javě podporován i pro hodnoty proměnné
String, a to od Javy 7.












